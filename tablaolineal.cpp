#include <iostream>
#include <iomanip> // Para setw
using namespace std;

class persona
{
	string nombre, direccion;
	
public:
	persona()
	{
		nombre = "asdasdasd";
		direccion = "asdasdasdasdasd";
	}
	persona(string n, string d)
	{
		this->nombre = n;
		this->direccion = d;
	}
	
	string get_nombre() const
	{
		return nombre;
	}
	string get_direccion() const
	{
		return direccion;
	}
	void set_nombre(string n)
	{
		nombre = n;
	}
	void set_direccion(string d)
	{
		direccion = d;
	}
	void mostrarDatosBase() const
	{
		cout << "Nombre: " << get_nombre() << endl;
		cout << "Direccion: " << get_direccion() << endl;
	}
};

class estudiante : public persona
{
	string cod_estudiante;
	string carrera;
	
public:
	estudiante()
	{
		cod_estudiante = "sin codigo";
		carrera = "sin nombre carrera";
	}
	estudiante(string n, string car, string d, string c) : persona(n, d)
	{
		this->cod_estudiante = c;
		this->carrera = car;
	}
	string get_cod_estudiante() const
	{
		return cod_estudiante;
	}
	string get_carrera() const
	{
		return carrera;
	}
	void set_cod_estudiante(string e)
	{
		cod_estudiante = e;
	}
	void set_carrera(string c)
	{
		carrera = c;
	}
	void mostrarDatosBase() const
	{
		cout << "Nombre: " << get_nombre() << endl;
		cout << "Codigo Estudiante: " << get_cod_estudiante() << endl;
		cout << "Carrera: " << get_carrera() << endl;
	}
};

void mostrarDatos(const persona& p, const estudiante& e, bool esLineal) {
	cout << "Datos de la persona:\n";
	if (!esLineal) {
		cout << "+-----------------------------+" << endl;
	}
	p.mostrarDatosBase();
	if (!esLineal) {
		cout << "+-----------------------------+" << endl;
	}
	cout << endl;
	cout << "Datos del estudiante:\n";
	if (!esLineal) {
		cout << "+-----------------------------------------+" << endl;
	}
	e.mostrarDatosBase();
	if (!esLineal) {
		cout << "+-----------------------------------------+" << endl;
	}
}

int main()
{
	char respuesta, formato;
	bool salir = false;
	bool esLineal = false;
	
	persona p1, p2("Primo Ramirez", "Calle los mangales");
	estudiante e1, e2("nom1", "car1", "Calle los mangales", "123");
	
	while (!salir) {
		cout << "�Desea imprimir los datos? (s/n): ";
		cin >> respuesta;
		
		if (respuesta == 's' || respuesta == 'S') {
			cout << "�En qu� formato desea imprimir los datos? (l/b): ";
			cin >> formato;
			esLineal = (formato == 'l' || formato == 'L');
			
			mostrarDatos(p2, e2, esLineal);
		} else if (respuesta == 'n' || respuesta == 'N') {
			cout << "Saliendo del programa..." << endl;
			salir = true;
		} else {
			cout << "Respuesta no valida. Por favor responda con 's' o 'n'." << endl;
		}
	}
	
	return 0;
}
